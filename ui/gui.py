import os
import threading
from tkinter import *
from tkinter.font import Font

from launcher import main_comparator_loop
from ui.checkbar import Checkbar
from ui.infodialog import infoDialog
from utils.classifiers.scores import getScores
from utils.plot_frame import getPlotModes, parsePlotRequestV2
from utils.classifiers.classifiers_core import launchClassifier, init_classifier_env, getModels
from utils.classifiers.dataset_maker import init_dataset_env, getDatasets


# dialog styles
dialog_outer_bg = "#FFFFFC"
dialog_inner_bg = "#F6F7F7"

default_delete_chance = 0.1
default_noise_mutator = 1
default_mode = "1-v-all"
default_family_size = 1000
default_trainig_set_percent_size = 0.3

# default padding values for the main layout
DEFAULT_X_PAD = 5
DEFAULT_Y_PAD = 5

titles_color = "#747374"

frame_0_color = "#F9FBFD"
frame_1_color = "#EFF0FE"
frame_2_color = "#F1EDEC"
frame_3_color = '#E2E9DF'
frame_4_color = "#FBFAE5"

# default title
application_title = "ALW - Machine Learning Comparator"

class Application(Frame):

    def __init__(self, master_0=None):
        super().__init__(master_0)

        # used to launch simple instance or full dataset solvers
        self.helper_buttons_list = []
        self.console_line_index = 0

        # utils
        self.all_results = None

        # local setup parameters
        highlightbackground = "gray"
        highlightcolor = "gray"
        highlightthickness = "2"
        bd = 0

        self.config(bg=frame_0_color)

        # 1 and slot for subframes
        self.master_1 = Frame(self)
        self.master_1_a = Frame(self.master_1)
        self.master_1_b = Frame(self.master_1)
        self.master_1_a.pack(anchor='nw', expand=1, fill=X)
        self.master_1_b.pack(anchor='nw', expand=1, fill=X)
        self.master_1.config(highlightbackground= highlightbackground, highlightcolor = highlightcolor, highlightthickness=highlightthickness, bd=bd)

        # 2 and slot for subframes
        self.master_2 = Frame(self)
        self.master_2_a = Frame(self.master_2)
        self.master_2_b = Frame(self.master_2)
        self.master_2_c = Frame(self.master_2)

        self.master_2_a.pack(anchor='nw', expand=1, fill=X)
        self.master_2_b.pack(anchor='nw', expand=1, fill=X)
        self.master_2_c.pack(anchor='nw', expand=1, fill=X)
        self.master_2.config(highlightbackground= highlightbackground, highlightcolor = highlightcolor, highlightthickness=highlightthickness, bd=bd)

        # user help guides
        guide_section_1 = self.readFile("utils/guide/guide_section_1.txt")
        guide_section_2 = self.readFile("utils/guide/guide_section_2.txt")

        # setup frame 1
        self.create_options_block_title(self.master_1_a, frame_1_color, "Instance setup", guide_section_1)
        self.create_drop_down_widgets(self.master_1_b,frame_1_color)

        # setup frame 2
        self.create_options_block_title(self.master_2_a, frame_2_color, "Output console",guide_section_2)
        self.create_text_view_toolbar(self.master_2_b, frame_2_color)
        self.create_text_view(self.master_2_c, frame_2_color)

        # final setup
        self.master_1.grid(row=1, column = 0, columnspan=2, sticky="ew",  padx=DEFAULT_X_PAD, pady=DEFAULT_Y_PAD)
        self.master_2.grid(row=0, column = 0, sticky="ew",  padx=DEFAULT_X_PAD, pady=DEFAULT_Y_PAD)

        self.grid()


    """
        Minor util, to read files
    """
    def readFile(self, filename):
        f = open(filename,'r')
        c = f.read()
        f.close()

        return c

    def create_drop_down_widgets(self, target_frame,option_color):

        target_frame.config(bg=option_color)
        target_frame.grid_columnconfigure(0, weight=0)
        target_frame.grid_columnconfigure(1, weight=1)

        target_frame.config(bg=option_color)
        self.label_dataset = Label(target_frame, text="Dataset", width=10, bg=option_color)
        self.label_dataset.grid(row=0, sticky="ew")

        self.checkbar1 = Checkbar(target_frame, background_color=option_color, maxDelivererButtonsListIniter = getDatasets())
        self.checkbar1.setAllTo(False)
        self.checkbar1.grid(row=0, column=1, sticky="ew")

        target_frame.config(bg=option_color)
        self.label_model = Label(target_frame, text="Model", width=10, bg=option_color)
        self.label_model.grid(row=1, sticky="ew")

        self.checkbar2 = Checkbar(target_frame, background_color=option_color, maxDelivererButtonsListIniter = getModels())
        self.checkbar2.setAllTo(False)
        self.checkbar2.grid(row=1, column=1, sticky="ew")

        target_frame.config(bg=option_color)
        self.label_score_name = Label(target_frame, text="Scores", width=10, bg=option_color)
        self.label_score_name.grid(row=2, sticky="ew")

        self.checkbar3 = Checkbar(target_frame, background_color=option_color, maxDelivererButtonsListIniter = getScores())
        self.checkbar3.setAllTo(False)
        self.checkbar3.grid(row=2, column=1, sticky="ew")

        self.label_plot = Label(target_frame, text="Plot Request", width=10, bg=option_color)
        self.label_plot.grid(row=3, sticky="ew")

        self.plot_option_selected = StringVar(self)
        self.plot_option_list = getPlotModes()
        self.plot_option_menu = OptionMenu(target_frame, self.plot_option_selected, *self.plot_option_list)
        self.plot_option_menu.config(bg=option_color)
        self.plot_option_menu.grid(row=3, column=1, sticky="ew")
        self.instance_bar_target_frame = target_frame
        self.plot_option_selected.set(self.plot_option_list[0])

        self.button_plot = Button(target_frame)
        self.button_plot["text"] = "Plot"
        self.button_plot["command"] = self.plotter
        self.button_plot.grid(row=4, sticky="ew",padx=20, pady=20)
        self.button_plot.config(state=DISABLED, bg=option_color,
                                highlightthickness=1,
                                highlightbackground=option_color,
                                highlightcolor=option_color)


        self.plot_option_menu.config(state=DISABLED)
        self.label_dataset.config(fg="gray")
        self.label_model.config(fg="gray")
        self.label_plot.config(fg="gray")
        self.label_score_name.config(fg="gray")

    def create_text_view_toolbar(self, target_frame, option_color):

        target_frame.config(bg=option_color)

        Label(target_frame, text="", bg=option_color ).grid(row=0, column = 0, padx=15, sticky="w",pady=0)
        self.noise_label = Label(target_frame, text="Noise multiplier", bg=option_color)
        self.noise_label.grid(row=0, column = 1, padx=0, sticky="e", pady=0)

        # input validation pattern
        vcmd = (self.register(self.validate_float_input),
                '%d', '%i', '%P', '%s', '%S', '%v', '%V', '%W')

        self.entry_noise = Entry(target_frame)
        self.entry_noise.insert(0, default_noise_mutator)
        self.entry_noise.config(width=20, validate = 'key' , validatecommand = vcmd,
                                         highlightthickness=1,
                                         highlightbackground=option_color,
                                         highlightcolor=option_color)
        self.entry_noise.grid(row=0, column = 2, padx=15, sticky="e", pady=0)


        Label(target_frame, text="", bg=option_color ).grid(row=1, column = 0, padx=15, sticky="w",pady=0)
        self.label_del = Label(target_frame, text="Data deletion percentage", bg=option_color)
        self.label_del.grid(row=1, column = 1, padx=0, sticky="e", pady=0)

        # input validation pattern
        vcmd = (self.register(self.validate_float_input),
                '%d', '%i', '%P', '%s', '%S', '%v', '%V', '%W')

        self.entry_del = Entry(target_frame)
        self.entry_del.insert(0, default_delete_chance)
        self.entry_del.config(width=20, validate = 'key' , validatecommand = vcmd,
                                         highlightthickness=1,
                                         highlightbackground=option_color,
                                         highlightcolor=option_color)
        self.entry_del.grid(row=1, column = 2, padx=15, sticky="e", pady=0)



        Label(target_frame, text="", bg=option_color ).grid(row=2, column = 0, padx=15, sticky="w",pady=0)
        self.label_train_set = Label(target_frame, text="Training set percentage", bg=option_color)
        self.label_train_set.grid(row=2, column = 1, padx=0, sticky="e", pady=0)

        # input validation pattern
        vcmd = (self.register(self.validate_float_input),
                '%d', '%i', '%P', '%s', '%S', '%v', '%V', '%W')

        self.entry_train_set = Entry(target_frame)
        self.entry_train_set.insert(0, default_trainig_set_percent_size)
        self.entry_train_set.config(width=20, validate = 'key' , validatecommand = vcmd,
                                         highlightthickness=1,
                                         highlightbackground=option_color,
                                         highlightcolor=option_color)
        self.entry_train_set.grid(row=2, column = 2, padx=15, sticky="e", pady=0)


        Label(target_frame, text="", bg=option_color ).grid(row=3, column = 0, padx=15, sticky="w",pady=0)
        self.label_fam = Label(target_frame, text="Family size", bg=option_color)
        self.label_fam.grid(row=3, column = 1, padx=0, sticky="e", pady=0)

        # input validation pattern
        vcmd = (self.register(self.validate_integer_input),
                '%d', '%i', '%P', '%s', '%S', '%v', '%V', '%W')

        self.entry_fam = Entry(target_frame)
        self.entry_fam.insert(0, default_family_size)
        self.entry_fam.config(width=20, validate = 'key' , validatecommand = vcmd,
                                         highlightthickness=1,
                                         highlightbackground=option_color,
                                         highlightcolor=option_color)
        self.entry_fam.grid(row=3, column = 2, padx=15, sticky="e", pady=0)


        Label(target_frame, text="", bg=option_color ).grid(row=4, column = 0, padx=15, sticky="w",pady=0)
        self.label_train = Label(target_frame, text="Train mode", width=10, bg=option_color)
        self.label_train.grid(row=4, column=1, sticky="e")

        self.mode_option_selected = StringVar(self)
        self.mode_option_list = ["1-v-all", "default"]
        self.mode_option_menu = OptionMenu(target_frame, self.mode_option_selected, *self.mode_option_list)
        self.mode_option_menu.config(bg=option_color, width=10)
        self.mode_option_menu.grid(row=4, column=2, sticky="w")
        self.mode_option_selected.set(self.mode_option_list[0])

        Label(target_frame, text="", bg=option_color ).grid(row=5, column = 0, padx=15, sticky="w",pady=0)
        self.label_refil = Label(target_frame, text="Refill mode", width=10, bg=option_color)
        self.label_refil.grid(row=5, column=1, sticky="e")

        self.refil_option_selected = StringVar(self)
        self.refil_option_list = ["mean", "knn", "mice"]
        self.refil_option_menu = OptionMenu(target_frame, self.refil_option_selected, *self.refil_option_list)
        self.refil_option_menu.config(bg=option_color, width=10)
        self.refil_option_menu.grid(row=5, column=2, sticky="w")
        self.refil_option_selected.set(self.refil_option_list[0])

        Label(target_frame, text="", bg=option_color ).grid(row=6, column = 0, padx=15, sticky="w",pady=0)
        self.label_distr = Label(target_frame, text="Noise dstr.", width=10, bg=option_color)
        self.label_distr.grid(row=6, column=1, sticky="e")

        self.distr_option_selected = StringVar(self)
        self.distr_option_list = ["exp", "uni"]
        self.distr_option_menu = OptionMenu(target_frame, self.distr_option_selected, *self.distr_option_list)
        self.distr_option_menu.config(bg=option_color, width=10)
        self.distr_option_menu.grid(row=6, column=2, sticky="w")
        self.distr_option_selected.set(self.distr_option_list[0])

        self.button_train = Button(target_frame)
        self.button_train["text"] = "Train"
        self.button_train["command"] = self.run_thread_train
        self.button_train.grid(row=7, sticky="ew",padx=20, pady=20)
        self.button_train.config(bg=option_color,
                                highlightthickness=1,
                                highlightbackground=option_color,
                                highlightcolor=option_color)

    def validate_integer_input(self, action, index, value_if_allowed,
                               prior_value, text, validation_type, trigger_type, widget_name):
        # action=1 -> insert
        if (action == '1'):
            if text in '0123456789':
                return True
            else:
                return False
        else:
            return True

    def validate_float_input(self, action, index, value_if_allowed,
                       prior_value, text, validation_type, trigger_type, widget_name):
        # action=1 -> insert
        if (action == '1'):
            if text in '0123456789, .':
                return True
            else:
                return False
        else:
            return True

    def create_text_view(self, target_frame, option_color):

        target_frame.config(bg=option_color)

        text_area_background_color = "#FDFAF5"
        text_area_border_color = "#252525"
        text_area_text_color = "#373736"
        highlightbackground= text_area_border_color
        highlightcolor = text_area_border_color
        highlightthickness = 1

        self.textWidjet = Text(target_frame, height="5", bg=text_area_background_color, highlightthickness = highlightthickness, fg=text_area_text_color)
        self.textWidjet.pack(padx=20, anchor='nw', expand=1, fill=X)
        self.textWidjet.config(state=DISABLED, highlightbackground= highlightbackground, highlightcolor = highlightcolor)

        Label(target_frame, text="", bg=option_color).pack(side="left")

    def create_options_block_title(self, target_frame, option_color, title, help_button_infos = None):

        target_frame.config(bg=option_color)
        boldFont = Font(size=15, weight="bold")
        Label(target_frame, text=title, bg=option_color, font=boldFont, fg=titles_color).pack(side="left", expand=TRUE)

        # don't add button if helper is not defined
        if(help_button_infos == None):
            return

        highlightbackground= option_color
        highlightcolor = option_color
        highlightthickness = 1

        helper = Button(target_frame, text="?", bg=option_color,
               highlightbackground=highlightbackground,
               highlightcolor=highlightcolor,
               highlightthickness = highlightthickness)

        def help_button_command():
            infoDialog(self,title, help_button_infos, dialog_inner_bg, dialog_outer_bg)

        helper.config(command=help_button_command)
        helper.pack(side="right", padx=5)

        self.helper_buttons_list.append(helper)

    def clear_output(self):
        self.textWidjet.config(state=NORMAL)
        self.textWidjet.delete('1.0', END)
        self.textWidjet.config(state=DISABLED)
        self.button_plot.config(state=DISABLED)

    def write_to_output(self, text):
        self.textWidjet.config(state=NORMAL)
        self.textWidjet.insert(END, text + "\n")
        self.textWidjet.config(state=DISABLED)
        self.textWidjet.see(END)

    def write_to_output_clear(self, text):
        self.clear_output()
        self.write_to_output(text)

    def main_comparator_loop_UI(
                            self,
                            used_mode='1-v-all',
                            training_set_percentage=0.3,
                            family_size_=1000,
                            delete_chance_=0.0,
                            scale_factor_=1,
                            refill_algorithm = "mean",
                            noise_distribution='exp'):
        all_scores = []

        datasets = getDatasets()
        models = getModels()

        total = len(datasets) * len(models)
        current = 0

        # for each dataset
        for dataset in datasets:

            # init dataset build env
            init_dataset_env(
                family_size_=family_size_,
                noise_distribution_=noise_distribution,
                dataset_name_=dataset,
                delete_chance_=delete_chance_,
                scale_factor_=scale_factor_,
                refill_algorithm_=refill_algorithm)

            # compare the modes
            for model in models:
                # init classifiers env
                init_classifier_env(
                    name_=model,
                    test_percentage_=training_set_percentage,
                    mode_=used_mode)

                string_msg = "Training: " + str(((current / total) * 100) // 1) + "%"

                self.write_to_output_clear(string_msg)

                # save dataset, model, scores
                scores = [dataset, model, launchClassifier()]

                # save for return
                all_scores.append(scores)

                current += 1

        return all_scores

    def train_all(self):

        self.clear_output()

        # disable ui
        self.plot_option_menu.config(state=DISABLED)
        self.button_plot.config(state=DISABLED)
        self.label_dataset.config(fg="gray")
        self.label_model.config(fg="gray")
        self.label_plot.config(fg="gray")
        self.label_score_name.config(fg="gray")
        self.checkbar1.setAllTo(False)
        self.checkbar2.setAllTo(False)
        self.checkbar3.setAllTo(False)

        delval = self.entry_del.get()
        noise = self.entry_noise.get()
        family_size = self.entry_fam.get()
        train_mode = self.mode_option_selected.get()
        refill_alg = self.refil_option_selected.get()
        distr = self.distr_option_selected.get()

        try:
            # train all
            self.all_results = self.main_comparator_loop_UI(
                used_mode = train_mode,
                delete_chance_= float(delval),
                scale_factor_= int(noise),
                family_size_= int(family_size),
                refill_algorithm=refill_alg,
                noise_distribution=distr)

            self.write_to_output_clear('All done!')

            # disable ui
            self.plot_option_menu.config(state=NORMAL)
            self.button_plot.config(state=NORMAL)
            self.label_dataset.config(fg="black")
            self.label_model.config(fg="black")
            self.label_plot.config(fg="black")
            self.label_score_name.config(fg="black")
            self.checkbar1.setAllTo(True)
            self.checkbar2.setAllTo(True)
            self.checkbar3.setAllTo(True)

        except:
            self.write_to_output("An error occourred!")

    def plotter(self):

        index = self.plot_option_list.index(self.plot_option_selected.get())

        datasets = self.checkbar1.getSelected()
        parsed_datasets = '.'.join(datasets)

        score = self.checkbar3.getSelected()
        score = score[0] + " score"

        models = self.checkbar2.getSelected()
        model = models[0]
        parsed_models = '.'.join(models)

        payload = None

        try:
            if(index == 0 or index == 2):
                payload = parsed_models + "-" + parsed_datasets

            if(index == 3):
                payload = model + "-" + parsed_datasets

            if(index == 1):
                payload = score + "-" + parsed_datasets
        except:
            payload = None

        if(payload != None):
            parsePlotRequestV2(self.all_results, index, payload)
        else:
            self.write_to_output_clear("An error occourred!")

    def run_thread_train(self):
        t = threading.Thread(target=self.train_all)
        # thread dies when main thread (only non-daemon thread) exits.
        t.daemon = True
        t.start()

# gui entry point
def launch_gui():
    root = Tk()
    root.title(application_title)
    root.resizable(width=False, height=False)

    app = Application(master_0=root)
    app.mainloop()

# if launched from here, you ask for gui popup
if __name__ == '__main__':
    launch_gui()