from tkinter import *
from tkinter.font import Font


class infoDialog:

    def __init__(self, parent, title, text, dialog_inner_bg, dialog_outer_bg):
        self.DIALOG_SIZE = 800

        self.dlg = Toplevel(master=parent)
        self.dlg.transient(parent)  # only one window in the task bar
        self.dlg.grab_set()         # modal
        self.dlg.title("User guide")


        highlightbackground = "gray"
        highlightcolor = "gray"
        highlightthickness = "2"
        bd = 0

        self.inner_frame = Frame(self.dlg)
        self.inner_frame.config(bg=dialog_inner_bg,
                                highlightbackground= highlightbackground,
                                highlightcolor = highlightcolor,
                                highlightthickness=highlightthickness,
                                bd=bd)
        self.inner_frame.pack(padx=10, pady=10)

        self.dlg.resizable(width=False, height=False)
        self.dlg.config(bg=dialog_outer_bg, width=self.DIALOG_SIZE)

        highlightbackground= dialog_inner_bg
        highlightcolor = dialog_inner_bg
        highlightthickness = 1

        boldFont = Font(size=15, weight="bold")
        Label(self.inner_frame, bg=dialog_inner_bg, text=title, font = boldFont, justify=CENTER).pack(padx=10, pady=0)

        Label(self.inner_frame, bg=dialog_inner_bg, text=text, wraplength=self.DIALOG_SIZE, justify=LEFT).pack(padx=10, pady=10)
        Button(self.inner_frame, padx=30, pady=10, text="OK", command=self.ok,
               highlightbackground=highlightbackground,
               highlightcolor=highlightcolor,
               highlightthickness = highlightthickness).pack(side="right",padx=10, pady=10)

    def ok(self):
        self.dlg.destroy()
