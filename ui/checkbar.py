
from tkinter import *

class Checkbar(Frame):
    def __init__(self, parent=None, maxRowLen= 6, deliverers = 0, side=LEFT, anchor=W, background_color = None, maxDelivererButtonsListIniter = None):

        Frame.__init__(self, parent)

        helpFrame = Frame(self)

        self.vars = []
        self.checkButtons = []
        counter = 0

        self.max_deliverers_available_in_all_instances = deliverers

        if (background_color != None):
            self.config(bg=background_color)

        self.maxDelivererButtonsList = list(range(1,self.max_deliverers_available_in_all_instances + 1))

        if(maxDelivererButtonsListIniter!= None):
            for element in maxDelivererButtonsListIniter:
                self.maxDelivererButtonsList.append(element)

        # generate list
        for pick in self.maxDelivererButtonsList:
            var = IntVar()
            var.set(0)
            chk = Checkbutton(helpFrame, text=pick, variable=var, width=10)

            if(background_color != None):
                chk.config(bg=background_color)

            chk.pack(side=side, anchor=anchor)
            self.vars.append(var)
            self.checkButtons.append(chk)
            counter = counter + 1

            if(counter == maxRowLen):
                helpFrame.pack(side="top", anchor=W)
                helpFrame = Frame(self)
                counter = 0

        if(counter != 0):
            helpFrame.pack(side="top", anchor=W)



    def setEnabledCheckboxes(self, amount, autoUncheck = True):
        for i in range(self.max_deliverers_available_in_all_instances + 1):
            if(i <= amount):
                self.checkButtons[i].config(state=NORMAL)
            else:
                self.checkButtons[i].config(state=DISABLED)
                if(autoUncheck):
                    self.vars[i].set(0)

    def setAllTo(self, setVal):
        for i in range(len(self.maxDelivererButtonsList)):
            if (setVal):
                self.checkButtons[i].config(state=NORMAL)
            else:
                self.checkButtons[i].config(state=DISABLED)

    def getSelected(self):
        selected = []

        for i in range(len(self.maxDelivererButtonsList)):
            if (self.vars[i].get() == 1):
                selected.append(self.maxDelivererButtonsList[i])

        if(len(selected) > 0):
            return selected
        else:
            return self.maxDelivererButtonsList

    def state(self):
        return map((lambda var: var.get()), self.vars)

