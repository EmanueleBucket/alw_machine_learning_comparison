

"""
    Note this script needs 'matplotlib' to be installed
"""
import matplotlib
matplotlib.use("TkAgg")
from matplotlib import pyplot as plt

from utils.classifiers.dataset_maker import getStatusLabel

# support parameters
plotted = 0
plot_symbol = '--'
colors_list = ['r', 'g', 'b', 'c', 'm', 'y', 'k']

"""
    Main plotting call
"""
def plotSetup(title, xlabel_, ylabel_):

    global plotted
    plotted = 0

    # close fig if exists
    if plt.get_fignums():
        plt.close()

    plt.xlabel(xlabel_)
    plt.ylabel(ylabel_)
    plt.title(title + " " + getStatusLabel())
    plt.grid(True)

    fig = plt.gcf()
    fig.canvas.set_window_title(title=title)

    # should not be a potential fullscreen
    fig.set_size_inches(11, 9)

    # fix spaces
    plt.subplots_adjust(left=None, bottom=0.14, right=None, top=0.93, wspace=None, hspace=None)

"""
    Add a new plot
"""
def plotAddPlot(dataX, dataY, algorithmName):

    global plotted

    # use static color
    if(plotted < len(colors_list)):
        colorform = colors_list[plotted]
        plt.plot(dataX, dataY, colorform + plot_symbol, label=algorithmName)
    else:
        plt.plot(dataX, dataY, plot_symbol, label=algorithmName)

    plotted = plotted + 1

"""
    Add barplots from a list of [name, value], ...
"""
def barPlots(plot_list, tilt_value = 0, fontsize=10, loop_color_index = -1):

    plot_color_index = 0
    print(plot_list)

    for element in plot_list:

        # get current color
        colorform = colors_list[plot_color_index]

        if(loop_color_index > 0):
            plt.bar(element[0], element[1], color = colorform)
        else:
            plt.bar(element[0], element[1])

        # move to next color
        plot_color_index += 1

        # reset if reached this value
        if(plot_color_index > loop_color_index or plot_color_index > len(colors_list)):
            plot_color_index = 0

    plt.xticks(rotation=tilt_value, fontsize=fontsize)


"""
    Add barplot from a single tuple [name, value]
"""
def barPlotsFill(tuple):
    plt.bar(tuple[0], tuple[1])

"""
    Final enabler
"""
def showAll(AddLegend = True):
    if(AddLegend):
        plt.legend(loc='upper right', ncol=1)
    plt.show()

