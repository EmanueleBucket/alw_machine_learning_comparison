
from utils.classifiers.classifiers_core import getModels
from utils.classifiers.dataset_maker import getDatasets
from utils.plotter import *

plotModes = [
        "Full dataset averaged scores over at least two model",
        "Dataset averaged score over all models for specific metric",
        "Roc for at least two model averaged labelwise",
        "Roc for a single model labelwise matching all datasets"]

"""
    Getter for plotmodels
"""
def getPlotModes():
    return plotModes


"""
    Small script to directly plot roc curve from its returned scores
"""
def plotRoc(scores):

    plotSetup("ROC Curve", 'False positives', 'True positives')

    for score in scores:

        roc = score[1][len(score[1]) - 1]
        tp = list(roc[1][0])
        fp = list(roc[1][1])

        # pass data
        plotAddPlot(fp, tp, score[0])

    # show all
    showAll()


"""
    Small script to direcly plot roc curve from an all scores list
"""
def plotAllRocForModel(all_scores, model_name, dataset_names_list):

    plotSetup("ROC Curve For Model " + model_name, 'False positives', 'True positives')

    # for each
    for score in all_scores:

        # valid score
        if (score[1] == model_name and score[0] in dataset_names_list):

            # for each valid subscore
            for subscore in score[2]:

                roc_data = subscore[1][len(subscore[1])-1]

                tp = list(roc_data[1][0])
                fp = list(roc_data[1][1])

                # pass data
                plotAddPlot(fp, tp, subscore[0] + " " + score[0])

    # show all
    showAll()


"""
    Script used to average data from any metric beside roc and plot it against all models
"""
def plotMetricFromAllAveraged(all_scores, model_names_list, metric_name, datasetnamelist):

    plotSetup("Metric comparison for " + metric_name, 'Metric value', 'Model')

    plottable_data = []

    # for each model grab the score ROC
    for model in model_names_list:
        scores_for_model = grabScoresForModel(all_scores, model, datasetnamelist, metric_name)

        averaged_metric_score = getMeanForMetricScores(scores_for_model)

        plottable_data.append( [model, averaged_metric_score[2]])


    # plot overhaul scores gained for each score metric (but roc)
    barPlots(plottable_data)

    # show all
    showAll()


"""
    Function used to take the metric data list and mean merge it
"""
def getMeanForMetricScores(metric_scores_list):

    # dynamic sizeing and initing
    averaged_metric_score = []

    for i in range(len(metric_scores_list[0])):
        averaged_metric_score.append(0)

    # for each roc score (use the one at index 1, as our implementation is missing)
    for metric_score in metric_scores_list:

        # get values values
        for i in range(1, len(metric_score)):

            try:
                averaged_metric_score[i] += metric_score[i]
            except:
                averaged_metric_score[i] += 0

    # apply mean for the list
    for i in range(len(averaged_metric_score)):
        averaged_metric_score[i] = averaged_metric_score[i]/len(metric_scores_list)

  
    return averaged_metric_score


"""
    Script used to average data from roc results to plot roc curve for each model
"""
def plotRocFromAllAveraged(all_scores, model_names_list, datasetnamelist):

    plotSetup("ROC Curve", 'False positives', 'True positives')

    # for each model grab the score ROC
    for model in model_names_list:
        scores_for_model = grabScoresForModel(all_scores, model, datasetnamelist, 'Roc curve')

        averaged_roc_score = getMeanForRocScores(scores_for_model)

        tp = list(averaged_roc_score[0])
        fp = list(averaged_roc_score[1])

        # pass data
        plotAddPlot(fp, tp, model)

    # show all
    showAll()


"""
    Function used to take the roc data list and mean merge it
"""
def getMeanForRocScores(roc_scores_list):

    # dynamic sizeing and initing
    averaged_roc_score_x = []
    averaged_roc_score_y = []

    for i in range(len(roc_scores_list[0][1])):
        averaged_roc_score_x.append(0)
        averaged_roc_score_y.append(0)

    # for each roc score (use the one at index 1, as our implementation is missing)
    for roc_score in roc_scores_list:

        # get x values
        for i in range(len(roc_score[1][0])):
            averaged_roc_score_x[i] += roc_score[1][0][i]

        # get y values
        for i in range(len(roc_score[1][1])):
            averaged_roc_score_y[i] += roc_score[1][1][i]


    # apply mean for x list
    for i in range(len(averaged_roc_score_x)):
        averaged_roc_score_x[i] = averaged_roc_score_x[i]/len(roc_scores_list)

    # apply mean for y list
    for i in range(len(averaged_roc_score_x)):
        averaged_roc_score_y[i] = averaged_roc_score_y[i]/len(roc_scores_list)

    return [averaged_roc_score_x, averaged_roc_score_y]

"""
    Grab all metric values for a given model
"""
def grabScoresForModel(all_scores, model_name, dataset_names_list, score_name):

    scores_vector = []

    # for each
    for score in all_scores:

        # valid score
        if (score[1] == model_name and score[0] in dataset_names_list):

            # for each valid subscore
            for subscore in score[2]:

                for metric in subscore[1]:

                    # store the right score
                    if(metric[0] == score_name):

                        scores_vector.append(metric)

    return scores_vector

"""
    Plot scores as bar plots
"""
def plotScoresOverhaul(scores):

    # setup roc graph
    plotSetup('Scores Overhaul','Score name', 'Score value')

    # plot overhaul scores gained for each score metric (but roc)
    for score in scores:
        for i in range(len(score[1]) - 1):
            barPlotsFill([score[1][i][0], score[1][i][2]])

    # show all
    showAll(False)


"""
    Plot scores comparison labelwise. Score name must be specified
"""
def compareScoreMetric(scores, metric_name):

    # setup roc graph
    plotSetup('Score comparison for ' + metric_name, 'Labels', 'Score value')

    # plot matching scores gained for each score metric (but roc)
    for score in scores:
        for i in range(len(score[1]) - 1):
            if(score[1][i][0] == metric_name):
                barPlotsFill([score[0], score[1][i][2]])

    # show all
    showAll(False)



"""
    Ciclic collector used to plot every score for every model averaged on every
    dataset used.
"""
def allScoresForAllModelsMean(all_scores, model_names_list, dataset_names_list):

    # setup roc graph
    plotSetup('Scores Overhaul', 'Score name', 'Score value')

    for model in model_names_list:
        allScoresForModelMean(all_scores, model, dataset_names_list, isAggregatingMode=True)

    # show all
    showAll(False)



"""
    Function used to plot all the scores for a given model averaged
    on the results gained in any matching dataset.
"""
def allScoresForModelMean(all_scores, model_name, dataset_names_list, isAggregatingMode = False):

    # build data grabber
    scores_count = len(all_scores[0][2][0][1]) - 1
    dataset_partial_scores = []

    # list of empy counters with names
    for i in range(scores_count):
        dataset_partial_scores.append(['init',0])

    different_datasets = 0

    mean_divisor_count = 1

    # for each
    for score in all_scores:

        # valid score
        if (score[1] == model_name and score[0] in dataset_names_list):

            # count different datasets
            different_datasets += 1

            mean_divisor_count = len(score[2])

            # for each subscore
            for subscore in score[2]:

                # for each subscore metric
                for i in range(len(subscore[1])):

                    # if available
                    if(subscore[1][i][2]  != "N/A"):

                        # here we point to our computed value for each score
                        dataset_partial_scores[i][1] += subscore[1][i][2]

                        if (not isAggregatingMode):

                            # add the label
                            dataset_partial_scores[i][0] = subscore[1][i][0]
                        else:

                            # minimize used space
                            parsed_score_name = subscore[1][i][0].split(" ")[0]

                            # add the label
                            dataset_partial_scores[i][0] = parsed_score_name + " " + model_name[:4]

    # apply mean
    for i in range(len(dataset_partial_scores)):
        dataset_partial_scores[i][1] = dataset_partial_scores[i][1]/ (different_datasets * mean_divisor_count)

    if(not isAggregatingMode):

        # setup roc graph
        plotSetup('Scores Overhaul For Model ' + model_name, 'Score name', 'Score value')

    if(isAggregatingMode):
        tilt_value = 75
    else:
        tilt_value = 0

    # plot overhaul scores gained for each score metric (but roc)
    barPlots(dataset_partial_scores, tilt_value=tilt_value, fontsize=7, loop_color_index=scores_count)

    if(not isAggregatingMode):

        # show all
        showAll(False)



"""
    Entry point for plot utils for single score
"""
def parsePlotRequest(scores, request):

    if(request == None):
        return

    plot_mode = request[0]
    plot_payload = request[1]

    # plot roc
    if (plot_mode == 0):
        plotRoc(scores)

    # plot overhaul results
    if (plot_mode == 1):
        plotScoresOverhaul(scores)

    # plot scores of a specific metric
    if (plot_mode == 2):
        compareScoreMetric(scores, plot_payload)



"""
    Entry point for plot utils for all scores
"""
def parsePlotRequestV2(all_scores, plot_request, plot_payload):

    full_model_list = getModels()
    model_or_metric_name = plot_payload.split("-")[0]

    # all models
    if (model_or_metric_name == '*'):
        model_or_metric_name = ".".join(full_model_list)

    # try parse the dataset
    try:
        datasetnamelist = plot_payload.split("-")[1]
    except:
        datasetnamelist = getDatasets()

    datasetnamelist = datasetnamelist.split(".")

    # try get the list
    try:
        models_list = model_or_metric_name.split(".")
    except:
        models_list = full_model_list


    # plot all scores for every model with mean aggregation
    if(plot_request == 0):
        allScoresForAllModelsMean(all_scores, models_list, datasetnamelist)

    # one metric vs all models (in this case the 'model_or_metric_name' variable is used to pass the metric name)
    if(plot_request == 1):
        plotMetricFromAllAveraged(all_scores, full_model_list, model_or_metric_name, datasetnamelist)

    # plot roc for all models averaged over the labels
    if (plot_request == 2):
        plotRocFromAllAveraged(all_scores, models_list, datasetnamelist)

    # plot roc for all labels for a single model
    if(plot_request == 3):
        plotAllRocForModel(all_scores, model_or_metric_name, datasetnamelist)

