
# list to array conversion
import numpy as np

"""
    Random classifier
"""
class RandomClassifier():

    # just store the input data
    def fit(self, X_train, y_train):
        self.X_train = X_train # unused
        self.y_train = y_train

    # randomly predict
    def predict(self, X_test):
        predictions = []

        for i in range(len(X_test)):
            label = np.random.choice(self.y_train)  # Random decision
            predictions.append(label)

        return predictions


