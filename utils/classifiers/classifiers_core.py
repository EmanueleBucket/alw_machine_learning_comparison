
# base line predictor
from utils.classifiers.random_classifier import RandomClassifier

# scores toolkit
from utils.classifiers.scores import compute_scores

# import a dataset
from sklearn import tree, neighbors, ensemble, neural_network

# partition into training and testing sets
from sklearn.model_selection import train_test_split

# our dataset
from utils.classifiers.dataset_maker import get_master_dataset

# used to obtain the dataset
from utils.classifiers.dataset_maker import oneVersusAll


full_model_list = ['tree', 'knn', 'random_forest', 'mlp', 'ada_boost']

"""
    Getter for model list
"""
def getModels():
    return full_model_list

# activity parameters
name = "tree"
test_percentage = 0.3
mode = "1-v-all"

"""
    Global parameters initer    
"""
def init_classifier_env(
                        name_ = "tree",
                        test_percentage_ = 0.3,
                        mode_ = "default"):

    global name
    global test_percentage
    global mode

    # overwrite them
    name = name_
    test_percentage = test_percentage_
    mode = mode_



"""
    Main solving routine
"""
def solve(dataset, classifier):

    # Can think of classifier as a function f(x) = y
    X = dataset.data
    y = dataset.target

    # test_size=0.5 -> split in half
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=test_percentage)

    # classifier passed as parameter
    classifier.fit(X_train, y_train)

    # predict
    predictions = classifier.predict(X_test)

    return [y_test, predictions]


"""
    Loop through all the possible labels
"""
def loopLaunch(classifier):

    scores = []

    boot_dataset = get_master_dataset()

    scores_mode = ""

    # for each label
    for i in range(len(boot_dataset.target_names)):

        shadow_dataset = None

        # obtain a one versus all version of the dataset
        if(mode == "1-v-all"):
            shadow_dataset = oneVersusAll(boot_dataset, i)
            scores_mode = "binary"

        # use the default dataset
        if(mode == "default"):
            shadow_dataset = boot_dataset
            scores_mode = "weighted"

        if(shadow_dataset != None):

            # solve it
            results = solve(shadow_dataset, classifier)

            # compute score metrics
            scores.append([boot_dataset.target_names[i], compute_scores(results[0], results[1], scores_mode)])

    return scores


"""
    Get a classifier instance
"""
def pickClassifier():

    # seen at lesson
    if(name == 'tree'):
        return tree.DecisionTreeClassifier()

    if(name == 'knn'):
        return neighbors.KNeighborsClassifier()

    if(name == 'mlp'):
        return neural_network.MLPClassifier(hidden_layer_sizes=(30, 30, 30,), activation="logistic")

    if(name == 'random_forest'):
        return ensemble.RandomForestClassifier()

    if(name == 'random'):
        return RandomClassifier()


    # newly found
    if(name == 'ada_boost'):
        return ensemble.AdaBoostClassifier()

    return None


"""
    All in one launcher for a classifier
"""
def launchClassifier():

    # pick classifier
    classifier = pickClassifier()

    # solve if possible
    if(classifier != None):
        return loopLaunch(classifier)

    else:
        return None








