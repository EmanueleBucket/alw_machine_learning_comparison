
# needed for Qfactor
from math import sqrt

# import all default scores
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score, roc_curve

scores_list = ["Accuracy", "Precision", "Recall", "Specificity", "F1", "Qfactor"]

"""
    Getter for the scores names list
"""
def getScores():
    return scores_list


"""
    Little script to obaint the true/false positives/negatives
"""
def perf_measure(y_true, y_pred):

    TP = 0
    FP = 0
    TN = 0
    FN = 0

    # compute TP, FP, TN, FN by hand
    for i in range(len(y_pred)):
        if y_true[i]==y_pred[i]==1:
           TP += 1
        if y_pred[i]==1 and y_true[i]!=y_pred[i]:
           FP += 1
        if y_true[i]==y_pred[i]==0:
           TN += 1
        if y_pred[i]==0 and y_true[i]!=y_pred[i]:
           FN += 1

    return [TP, FP, TN, FN]


""" 
    Get the score metrix by name
"""
def get_metrix(metric_name, y_true, y_pred):

    # mes = [TP, FP, TN, FN]
    mes = perf_measure(y_true, y_pred)
    try:
        if(metric_name == 'accuracy'):
            return (mes[0] + mes[2])/(mes[0]+mes[1]+mes[2]+mes[3])

        if(metric_name == 'precision'):
            return mes[0] / (mes[0] + mes[1])

        if(metric_name == 'recall'):
            return mes[0] / (mes[0] + mes[3])

        if(metric_name == 'specificity'):
            return mes[2] / (mes[1] + mes[2])

        if(metric_name == 'f1'):
            precision = get_metrix('precision',y_true, y_pred)
            recall = get_metrix('recall', y_true, y_pred)
            return (2 * precision * recall)/(precision+recall)

        if(metric_name == 'qf'):
            ey = mes[0]/(mes[0]+mes[3])
            ep = mes[1]/(mes[1]+mes[2])
            return ey/(sqrt(ep))

    except:
        return "N/A"

    return None


"""
    Display to console the scores value given a full vector
    of results taken from the full comparator run
"""
def printAllScores(all_scores):

    for scores in all_scores:


        print("")
        print("#########################################################")
        print("##                      RESULTS                        ##")
        print("#########################################################")

        print("\nOn Dataset ", scores[0], " and model ", scores[1])

        # print labelwise scores
        for score in scores[2]:
            print("Label: ", score[0])
            printScores(score[1])

"""
    Display to console the scores values
"""
def printScores(scores):

    for score in scores:
        if(score[0] != "Roc curve"):
            print('{:25} sk: {:20} | our: {:20}'.format(str(score[0]), str(score[1]), str(score[2])))
    print("\n\n-------------------------------")


"""
    Return a vector with all the currently available scores
"""
def compute_scores(y_true, y_pred, average = "binary"):

    scores = []

    # accuracy score
    scores.append(["Accuracy score", accuracy_score(y_true, y_pred), get_metrix('accuracy',y_true, y_pred)])

    # precision score
    scores.append(["Precision score", precision_score(y_true, y_pred, average=average), get_metrix('precision',y_true, y_pred)])

    # recall score
    scores.append(["Recall score", recall_score(y_true, y_pred, average=average), get_metrix('recall',y_true, y_pred)])

    # specificity score
    scores.append(["Specificity score", "N/A", get_metrix('specificity',y_true, y_pred)])

    # f1 score
    scores.append(["F1 score", f1_score(y_true, y_pred, average=average), get_metrix('f1',y_true, y_pred)])

    # Qf score
    scores.append(["Qfactor score", "N/A", get_metrix('qf',y_true, y_pred)])

    if(average == 'binary'):
        # roc curve
        scores.append(["Roc curve", roc_curve(y_true, y_pred, 0) , "N/A"])
    else:
        # roc curve
        scores.append(["Roc curve", "N/A", "N/A"])

    return scores

