# used to avoid global ovewrite
from copy import deepcopy

# bunch class for datasets
from sklearn.datasets.base import Bunch

# list to array conversion
import numpy as np

# import fancyimputer library imputers
from fancyimpute import SimpleFill, KNN, NuclearNormMinimization, MICE

#############################
# WEATHER DATASET           #
#############################

"""
    Columns menaing:
        - annual temperature 
        - annual average maximum temperature 
        - annual average minimum temperature
        - rain or snow precipitations 
        - annuall average wind speed
        - number of days with rains
        - number of days with snow
        - number of days with hail
"""
skeletonsMilan = [
    [15.00, 20.7, 9.80, 443.75, 5.80, 105.0, 4.00, 1],   # 2003
    [14.60, 19.7, 9.90, 440.00, 5.80, 111.0, 11.0, 0],   # 2004
    [14.50, 19.5, 9.80, 510.81, 5.40, 107.0, 5.00, 0],   # 2006
    [14.10, 19.2, 9.30, 461.75, 5.10, 134.0, 11.0, 3],   # 2008
    [14.30, 19.7, 9.20, 502.46, 5.10, 116.0, 14.0, 1],   # 2009
    [13.40, 18.0, 9.10, 884.22, 6.10, 147.0, 22.0, 1]    # 2010
]
skeletonsRome = [
    [18.04, 24.5, 10.3, 622.82, 7.50, 56.00, 0.00, 0],   # 2003
    [17.10, 21.6, 10.4, 707.93, 7.50, 82.00, 1.00, 3],   # 2004
    [15.10, 21.9, 8.80, 398.52, 5.30, 67.00, 0.00, 3],   # 2006
    [16.10, 21.5, 8.70, 618.76, 7.50, 81.00, 0.00, 2],   # 2008
    [17.40, 22.5, 10.2, 653.83, 8.90, 86.00, 1.00, 4],   # 2009
    [17.00, 22.2, 10.1, 321.87, 8.30, 88.00, 0.00, 1]    # 2010
]
skeletonsBari = [
    [16.0, 20.9, 11.20, 595.63, 12.2, 110.0, 7.00, 0],  # 2003
    [15.8, 20.4, 11.10, 667.52, 11.0, 111.0, 6.00, 0],  # 2004
    [15.5, 20.2, 10.90, 540.49, 11.3, 103.0, 2.00, 0],  # 2006
    [16.5, 21.1, 11.70, 605.07, 12.2, 88.00, 2.00, 0],  # 2008
    [16.3, 20.7, 11.70, 522.89, 12.3, 129.0, 3.00, 0],  # 2009
    [16.1, 20.4, 11.50, 670.54, 13.1, 143.0, 3.00, 0]   # 2010
]

# column names
skeletons_names_weather = ["2003", "2004", "2006", "2008", "2009", "2010"]

# master parameters for random noise
master_noise_weather = [1.7, 2.0, 2.0, 172.0, 1.0, 10, 1, 1]


#############################
# FIRES DATASET             #
#############################


"""
    Columns menaing:
        - Serving Size (g)
        - Calories
        - Total Fat (g)
        - Saturated Fat (g)
        - Trans Fat (g)
        - Carbs (g)
        - Sodium (mg)
"""
skeletonsFries = [
    [170.0, 570.0, 30.0, 6.0, 8.00, 70.0, 330.00], # McDonald's
    [160.0, 500.0, 28.0, 6.0, 6.00, 57.0, 820.00], # Burger King
    [170.0, 550.0, 31.0, 6.0, 10.0, 60.0, 1200.0], # Jack In The Box
    [213.0, 566.0, 37.0, 7.0, 1.00, 82.0, 1029.0], # Arby's
    [102.0, 260.0, 13.0, 2.5, 0.00, 33.0, 740.00], # KFC
    [125.0, 380.0, 19.0, 7.0, 0.00, 44.0, 600.00], # Sonic
    [156.0, 430.0, 18.0, 4.5, 5.50, 61.0, 640.00], # A&W
    [88.00, 310.0, 17.0, 7.0, 1.00, 35.0, 660.00]  # Popeyes
]

# column names
skeletons_names_fries = ["McDonald's", "Burger King","Jack In The Box", "Arby's","KFC", "Sonic", "A&W", "Popeyes"]

# master parameters for french fries
master_noise_fries = [30.0, 100.0, 7.0, 2.0, 4.0, 20.0, 150.0]







# global dataset indexer
datasets = ['Rome', 'Milan', 'Bari', 'Fries']

"""
    Getter for the datasets list
"""
def getDatasets():
    return datasets

# activity variables
family_size = 1000
noise_distribution = 'exp'
dataset_name = 'Rome'
delete_chance = 0.1
scale_factor = 1
refill_algorithm = 'means'

master_dataset = None

""" 
    Global parameters initer
"""
def init_dataset_env(
                        family_size_ = 1000,
                        noise_distribution_ = 'exp',
                        dataset_name_ = 'Rome',
                        delete_chance_ = 0.1,
                        scale_factor_ = 1,
                        refill_algorithm_ = 'means'):

    global noise_distribution
    global dataset_name
    global delete_chance
    global scale_factor
    global refill_algorithm
    global family_size

    # overwrite them
    noise_distribution = noise_distribution_
    dataset_name = dataset_name_
    delete_chance = delete_chance_
    scale_factor = scale_factor_
    refill_algorithm = refill_algorithm_
    family_size = family_size_


    # get dataset
    global master_dataset
    master_dataset = init_master_dataset()


"""
    Getter for the dataset
"""
def get_master_dataset():
    return deepcopy(master_dataset)

"""
    Small script to get a string descriptor of the current status
    for plot util.
"""
def getStatusLabel():
    return "N: " + str(family_size) + " α: " + str(delete_chance) + " , β: " + str(scale_factor) + " , refill: " + refill_algorithm

"""
    Routine used to add missing data to the dataset    
"""
def random_delete_data(base_dataset):

    # for each row
    for row in base_dataset:

        # for each element
        for i in range(len(row)):

            # toss a coin and replace the data with some missing entries with a missing data flag : 'None'
            if(np.random.uniform() < delete_chance):

                # set as missing data
                row[i] = np.nan

"""
    Build a one versus all version of the dataset
"""
def oneVersusAll(dataset, index):

    # avoid editing original datas
    shadow_dataset = deepcopy(dataset)

    # add the else field to the names array
    finalList = [shadow_dataset.target_names[index], 'else']
    shadow_dataset.target_names = np.asarray(finalList)

    # set everything different from the target index as else
    for i in range(len(shadow_dataset.target)):

        # if is different, overwrite it
        if shadow_dataset.target[i] != index:
            shadow_dataset.target[i] = 1  # else
        else:
            shadow_dataset.target[i] = 0  # original

    return shadow_dataset

"""
    Routine used to build a randomized family from a skeletor row
"""
def make_family(skeleton, noise_vector, family_size):

    skeleton_family = []
    for i in range(family_size):
        skeleton_family.append(skeleton.copy())

    for i in range(len(noise_vector)):

        noise = noise_vector[i]

        # uniform
        if (noise_distribution == "uni"):
            noise_list = list(np.random.uniform(-noise, noise, family_size))

            for j in range(len(skeleton_family)):
                row = skeleton_family[j]
                row[i] += noise_list[j]

        # exponential
        if (noise_distribution == "exp"):
            noise_list = list(np.random.exponential(noise, family_size))
            flip_sign = np.random.randint(2, size=family_size)

            for j in range(len(skeleton_family)):
                row = skeleton_family[j]

                # to add exponential negative noise, a 50% coin toss is needed
                if(flip_sign[j] == 0):
                    row[i] += noise_list[j]
                else:
                    row[i] -= noise_list[j]


    return skeleton_family


"""
    Apply the noise scale factor
"""
def scaled_noise():

    master_noise = []

    if(dataset_name == 'Fries'):
        master_noise = master_noise_fries

    if(dataset_name == 'Rome' or dataset_name == 'Milan' or dataset_name == 'Bari'):
        master_noise = master_noise_weather

    sc_noise = []
    for noise in master_noise:
        sc_noise.append(noise * scale_factor)

    return sc_noise


"""
    Main routine to obtain a new dataset from the skeleton base
"""
def generate_dataset(family_size):

    # output
    total_dataset = []

    # target set
    skeletons_set = []

    # scale noise by a constant given factor
    noise = scaled_noise()

    if dataset_name == "Rome":
        skeletons_set = skeletonsRome
    if dataset_name == "Milan":
        skeletons_set = skeletonsRome
    if dataset_name == "Bari":
        skeletons_set = skeletonsBari

    if dataset_name == "Fries":
        skeletons_set = skeletonsFries

    # build dataset
    for skeleton in skeletons_set:
        result = make_family(skeleton, noise, family_size)

        # save results
        for element in result:
            total_dataset.append(element)

    return total_dataset


"""
    This routine takes a hole-pinched dataset and fills it according to a given policy 
"""
def custom_fill(base_dataset, refill_algorithm):
    x_filled = None
    base_dataset = np.array(base_dataset)

    if refill_algorithm == "mice":
        x_filled = MICE().complete(base_dataset)

    if refill_algorithm == "knn":
        x_filled = KNN(k=3).complete(base_dataset)

    if refill_algorithm == "mean":
        x_filled = SimpleFill("mean").complete(base_dataset)

    return x_filled.tolist()


"""
    Generate the dataset and cast it to sklearn compatible format
"""
def init_master_dataset():

    # obtain data
    data = generate_dataset(family_size)

    # ignore too small delete chances or too big
    if(delete_chance > 0.005 and delete_chance < 1):

        # add missing data noise
        random_delete_data(data)

        # refill tata
        data = custom_fill(data, refill_algorithm)

    dataset = {}
    dataset["data"] = data

    families = len(data) // family_size

    target = []
    for i in range(families):
        for j in range(family_size):
            target.append(i)

    skeletons_names = ""

    if(dataset_name == 'Fries'):
        skeletons_names = skeletons_names_fries

    if(dataset_name == 'Rome' or dataset_name == 'Milan' or dataset_name == 'Bari'):
        skeletons_names = skeletons_names_weather

    return Bunch(data=data, target=target, target_names=skeletons_names)
