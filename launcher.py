# core
from utils.classifiers.classifiers_core import launchClassifier, init_classifier_env, getModels
from utils.classifiers.scores import printScores
from utils.classifiers.dataset_maker import init_dataset_env, getDatasets
from utils.plot_frame import parsePlotRequestV2

"""
    Conclusion call for results display
"""
def conclude(scores, model, test_percentage, mode):

    # print scores
    print("For algorithm  : ", model)
    print("Test percenage : ", test_percentage)
    print("Mode           : ", mode)
    print()

    # print labelwise scores
    for score in scores:

        print("Label: ", score[0])
        printScores(score[1])


"""
    Main setup call
"""
def setupRun():
    """"""

    """
        #######################################
        #         MODELS PARAMETERS           #
        #######################################
    """

    """
        Available models:
            tree                - simple tree
            knn                 - kNearNeighboor
            random_forest       - random forest
            mlp                 - multy layer perceptron
            ada_boost           - ada boost
            random              - random
    """
    model = 'random'

    """
        Available analysis modes:
            default             - each label is compared in macro mode
            1-v-all             - each label is compared to the others in one vs all mode
    """
    mode = "1-v-all"

    """
        This is the slice of the dataset that will be used to test the results
    """
    test_percentage = 0.2


    """
        #######################################
        #        DATASET PARAMETERS           #
        #######################################
    """

    """
        Available noise distributions:
            uni                 - uniform
            exp                 - exponential
    """
    distribution = 'exp'


    """
        This parameter will scale the noise vector used to build the dataset
    """
    scale_factor = 2

    """
        Available dataset location:
        Rome                    - Rome
        Milan                   - Milan
        Bari                    - Bari
    """
    dataset_name = "Rome"


    """
        Chance used to pinch holes (add missing entries) in the dataset with uniform distribution.
    """
    delete_chance = 0.2


    # init dataset build env
    init_dataset_env(
                        family_size_= 500,
                        noise_distribution_= distribution,
                        dataset_name_= dataset_name,
                        delete_chance_= delete_chance,
                        scale_factor_ = scale_factor)

    # init classifiers env
    init_classifier_env(
                        name_= model,
                        test_percentage_ = test_percentage,
                        mode_ = mode)

    # main launcher (third parameter is ask for roc plot)
    scores = launchClassifier()

    # finalize output
    conclude(scores, model, test_percentage, mode)

    return scores


"""
    Main data grab routine:
    For each model and for each dataset, obtain all the results and return them in a list with identification
    labels for later computation.
"""
def main_comparator_loop(
        used_mode = '1-v-all',
        training_set_percentage = 0.3,
        family_size_=1000,
        delete_chance_=0,
        scale_factor_= 1,
        refill_algorithm="mean"):

    all_scores = []

    datasets = getDatasets()
    models = getModels()

    total = len(datasets)*len(models)
    current = 0


    # for each dataset
    for dataset in datasets:

        # init dataset build env
        init_dataset_env(
            family_size_= family_size_,
            noise_distribution_='exp',
            dataset_name_=dataset,
            delete_chance_=delete_chance_,
            scale_factor_= scale_factor_,
            refill_algorithm_=refill_algorithm)


        # compare the modes
        for model in models:

            # init classifiers env
            init_classifier_env(
                name_=model,
                test_percentage_=training_set_percentage,
                mode_=used_mode)


            string_msg = '\r' + "Training: " +str(((current/total)*100)//1)+"%"
            print(string_msg, end='')

            # save dataset, model, scores
            scores = [dataset, model, launchClassifier()]

            # save for return
            all_scores.append(scores)

            current += 1

    print('\rAll done!', end='')

    return all_scores

"""
    Loop analysis function
"""
def loopAnalysis(all_results):

    reloop = True

    print("\n\nLegend: insert the plot type, and secondary inputs after)")
    print("0 : full dataset averaged scores over at least two model (*, tree.knn.mlp, tree.knn.mlp-Milan, etc...)")
    print("1 : dataset averaged score over all models for specific metric (Accuracy score, Recall score, Recall score-Milan, etc...)")
    print("2 : plot roc for a single model labelwise matching all datasets (tree, knn, knn-Rome, etc...)")
    print("3 : plot roc for every model averaged labelwise (*, tree, tree.knn.mlp, tree.knn.mlp-Milan, etc...)")

    while(reloop):

        try:
            plot_request = int(input("\nInsert plot request  >>> "))
            payload = input("Insert payload  >>> ")

            print("Processing request", plot_request, payload)

            # plot util
            parsePlotRequestV2(all_results, plot_request, payload)

        except:
            print("Something went wrong!")

        re = input("Plot again? y/n  >>> ")
        reloop = (re == 'y' or re == 'Y')

    print("Terminated")

# main launcher
if (__name__ == "__main__"):

    print("This launcher is deprecated")

    # get all possible datas
    all_results = main_comparator_loop()

    # see them
    # printAllScores(all_results)

    loopAnalysis(all_results)