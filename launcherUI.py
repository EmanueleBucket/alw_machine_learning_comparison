import sys

# check python 3 is enabled
if sys.version_info[0] < 3:
    print("Warning, this software requires the python 3 interpreter.")
    print("Please, visit 'https://www.python.org' and follow the instructions for the installation.")
    print("This process will require an internet connection.")

else:
    from ui.gui import launch_gui

    # if launched from here, you ask for gui popup
    if __name__ == '__main__':
        launch_gui()